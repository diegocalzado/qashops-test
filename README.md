# README #

Repositorio de test para qashops

### ¿Cómo probar el código? ###

* Los archivos descargados quedan en la carpeta /tmp
* Lanzar los test de phpunit usando el archivo de configuración /tests/functional-tests.xml
* Para probar la descarga en paralelo de ficheros es necesario instalar 
 [Beanstalkd](http://kr.github.io/beanstalkd/)
* Una vez instalado y corriendo en 127.0.0.1, levantar el worker /src/Borme/BormeWorker.php y lanzar a continuación el producer src/Borme/BormeProducer.php 

### Cosas a mejorar en el código ###

* Usar un contenedor de dependencias como por ejemplo el de [Symfony](http://symfony.com/doc/current/components/dependency_injection.html) para gestionar la creación de objetos y evitar la creación en los constructores, desacoplando mejor el código.
* Mayor control de errores.
* Mayores pruebas a las expresiones regulares.
* Más test funcionales y unitarios.
* Sistema de config por environment.
* Algún comentario más ;)