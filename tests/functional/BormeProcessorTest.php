<?php

/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 30/5/17
 * Time: 0:13
 */

use Borme\BormeProcessor;

class BormeProcessorTest extends BaseTest
{
    protected $text;

    /**
     * Comprobamos código para una cadena vacía
     */
    public function test01()
    {
        $this->prepareData01();

        $borme = new BormeProcessor();
        $entries = $borme->getBormeInformation($this->text);

        $this->assertTrue(count($entries) == 0);
    }

    protected function prepareData01()
    {
        $this->text = "";
    }

    /**
     * Comprobamos código para una cadena con 1 registro
     */
    public function test02()
    {
        $this->prepareData02();

        $borme = new BormeProcessor();
        $entries = $borme->getBormeInformation($this->text);

        $this->assertTrue(count($entries) == 1);

        foreach ($entries as $entry) {
            /** @var $entry \Borme\BormeEntry */
            $this->assertTrue($entry->getCompanyName() == 'AMATISTA SOLUTIONS SOCIEDAD LIMITADA.');
            $this->assertTrue($entry->getVolume() == '6102');
            $this->assertTrue($entry->getPage() == '218');
            $this->assertTrue($entry->getSection() == '8');
            $this->assertTrue($entry->getSheet() == 'SE107352');
        }
    }

    protected function prepareData02()
    {
       $this->text = "10314 - AMATISTA SOLUTIONS SOCIEDAD LIMITADA.
Modificaciones estatutarias. 9. NOMBRAMIENTO, DURACION Y PROHIBICION DE COMPETENCIA.-. Datos registrales. T 6102 ,F 218, S 8, H SE107352, I/A 5 (19.12.16).";
    }

    /**
     * Comprobamos el código para una cadena con múltiples registros, incluído el último cortado
     */
    public function test03()
    {
        $this->prepareData03();

        $borme = new BormeProcessor();
        $entries = $borme->getBormeInformation($this->text);

        $this->assertTrue(count($entries) == 7);

        // TODO (dCalzado): Chequear las entries, demasiado para el scope de la prueba
        foreach ($entries as $entry) {
            /** @var $entry \Borme\BormeEntry */
        }
    }

    protected function prepareData03()
    {
        $this->text = "BOLETÍN OFICIAL DEL REGISTRO MERCANTIL
Martes 10 de enero de 2017

SECCIÓN PRIMERA
Empresarios
Actos inscritos
SEVILLA
10314 - AMATISTA SOLUTIONS SOCIEDAD LIMITADA.
Modificaciones estatutarias. 9. NOMBRAMIENTO, DURACION Y PROHIBICION DE COMPETENCIA.-. Datos registrales. T 6102 ,F 218, S 8, H SE107352, I/A 5 (19.12.16).
10315 - PROMOCIONES URFERA SL.
Ceses/Dimisiones. Adm. Unico: INVERSIONES MURALLAS DEL SOL SOCIEDAD LIMITADA. Socio único: LAR INMUEBLES SL.Situación concursal. Procedimiento concursal 727/2016. FIRME: Si, Fecha de resolución 12/09/2016. Auto de declaración deconcurso. Voluntario. Juzgado: num. 1 JUZGADO DE LO MERCANTIL, Nº 1 DE SEVILLA. Juez: EDUARDO GOMEZ LOPEZ.Resoluciones: Se inscribe la declaración de concurso y su simultanea conclusión de la sociedad, así como su extinción y cancelaciónde la misma.- Situación concursal. Procedimiento concursal 727/2016. FIRME: Si, Fecha de resolución 12/09/2016. Auto deconclusión del concurso. Insuficiencia de la masa activa. Juzgado: num. 1 JUZGADO DE LO MERCANTIL, Nº 1 DE SEVILLA. Juez:EDUARDO GOMEZ LOPEZ. Resoluciones: Se inscribe la declaración de concurso y su simultanea conclusión de la sociedad, asícomo su extinción y cancelación de la misma.- Extinción. Datos registrales. T 5443 , F 150, S 8, H SE 21034, I/A 16 (23.12.16).
10316 - COLINAS ALJARAFE SL.
Situación concursal. Procedimiento concursal 2483/15. FIRME: Si, Fecha de resolución 10/10/2016. Auto de aprobación del plan deliquidación. Juzgado: num. 2 JUZGADO DE LO MERCANTIL Nº DE SEVILLA. Juez: PEDRO MARQUEZ RUBIO. Resoluciones: Seinscribe la aprobación del plan de liquidación y la formación de la sección de calificación. Datos registrales. T 3274 , F 54, S 8, H SE44596, I/A 11 (27.12.16).
10317 - CLUB DE CAMPO JABUGO SL.
Ceses/Dimisiones. Adm. Unico: MOLINA MENCHACA MARIA DEL ROSARIO. Situación concursal. Procedimiento concursal2519/2015. FIRME: Si, Fecha de resolución 5/09/2016. Auto de conclusión del concurso. Finalizacion de la fase de liquidacion.Juzgado: num. 2 JUZGADO DE LO MERCANTIL DE SEVILLA. Juez: PEDRO MARQUEZ RUBIO. Resoluciones: Inscripción de laconclusión del concurso, cese de la limitación de facultades,.cese de la administración concursal, extinción y cancelación. Situaciónconcursal. Procedimiento concursal 2519/2015. FIRME: Si, Fecha de resolución 5/09/2016. Cese de las limitaciones de las facultadesde administración. Juzgado: num. 2 JUZGADO DE LO MERCANTIL DE SEVILLA. Juez: PEDRO MARQUEZ RUBIO. Situaciónconcursal. Procedimiento concursal 2519/2015. Fecha de resolución 11/11/2015. Revocación de administradores concursales.Juzgado: num. 2 JUZGADO DE LO MERCANTIL DE SEVILLA. Juez: JUAN FRANCISCO SANTANA MIRALES. AdministradorConcursal. Fecha 14/09/2016, NIF/CIF 28717803A. SECO GORDILLO JUAN ANTONIO. Extinción. Datos registrales. T 3271 , F56, S 8, H SE 44501, I/A 5 (27.12.16).
10318 - LUGEC INVERSION SOLAR SL.
Reelecciones. Adm. Unico: WOC FOTOVOLTAICA ESPAÑA SL S COM. Fusión por absorción. Sociedades absorbidas:BONSOLAR DE LEVANTE SLU. WATIOS PICO DE LEVANTE SOCIEDAD LIMITADA. INVERCUBIERTAS DE LEVANTE SOCIEDADLIMITADA. Datos registrales. T 5208 , F 139, S 8, H SE 85765, I/A 5 (30.12.16).
10319 - SYNZE 2003 SOCIEDAD LIMITADA.
Ceses/Dimisiones. Adm. Unico: COBREROS MARTINEZ FRANCISCO MIGUEL. Nombramientos. LiqUnico: COBREROSMARTINEZ FRANCISCO MIGUEL. Otros conceptos: Cambio del Organo de Administración: Administrador único a LiquidadorUnico. Disolución. Voluntaria. Extinción. Datos registrales. T 4253 , F 16, S 8, H SE 64351, I/A 13 (28.12.16).
10320 - CALL CENTER LEMO SOCIEDAD LIMITADA.
Ceses/Dimisiones. LiqUnico: JUAN ANTONIO LEON MARTINEZ. Adm. Unico: LEON MARTINEZ JUAN ANTONIO.Nombramientos. LiqUnico: JUAN ANTONIO LEON MARTINEZ. Otros conceptos: Cambio del Organo de Administración:Administrador único a Liquidador Unico. Disolución. Voluntaria. Extinción. Datos registrales. T 5655 , F 197, S 8, H SE 96327, I/A2 (28.12.16).
10321 - DEHESA ASOMADILLA INVERSIONES SL.
Reelecciones. Adm. Unico: WOC FOTOVOLTAICA ESPAÑA SL S COM. Fusión por absorción. Sociedades absorbidas:BENEFITS DE VALVERDE 2008 SOCIEDAD LIMITADA. RENEX DE INVERSIONES SOCIEDAD LIMITADA. INVESOLAR VALE 2008SOCIEDAD LIMITADA. VALVERDE BUSSINES 2008 SOCIEDAD LIMITADA. AGRUPACION SOLAR VALVERDE SOCIEDAD ";
    }
}