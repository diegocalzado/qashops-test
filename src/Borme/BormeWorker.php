<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 31/5/17
 * Time: 1:04
 */

require_once __DIR__ . '/../../vendor/autoload.php';

use Pheanstalk\Pheanstalk;
use Borme\BormeDownloader;

$queue = new Pheanstalk("127.0.0.1");
$queue->watch('bormetube');

while ($job = $queue->reserve()) {
    $url = json_decode($job->getData(), true);
    $borme = new BormeDownloader();
    $borme->downloadBorme($url);
    $queue->delete($job);
}