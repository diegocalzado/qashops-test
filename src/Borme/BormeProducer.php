<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 30/5/17
 * Time: 22:00
 */

require_once __DIR__ . '/../../vendor/autoload.php';

use Pheanstalk\Pheanstalk;

$queue = new Pheanstalk('127.0.0.1');

$urls = array(
    'https://www.boe.es/borme/dias/2017/05/30/pdfs/BORME-A-2017-100-02.pdf',
    'https://www.boe.es/borme/dias/2017/05/30/pdfs/BORME-A-2017-100-03.pdf',
    'https://www.boe.es/borme/dias/2017/05/30/pdfs/BORME-A-2017-100-04.pdf',
    'https://www.boe.es/borme/dias/2017/05/30/pdfs/BORME-A-2017-100-06.pdf',
    'https://www.boe.es/borme/dias/2017/05/30/pdfs/BORME-A-2017-100-07.pdf',
);

foreach ($urls as $url) {
    $queue
        ->useTube('bormetube')
        ->put(json_encode($url));
}