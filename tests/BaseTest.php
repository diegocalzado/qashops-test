<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 29/05/17
 * Time: 22:46
 */

abstract class BaseTest extends PHPUnit_Framework_TestCase
{
    /**
     * Crea un mock de la clase indicada con metodos indicados.
     * @param $className
     * @param array $methodMaps
     * @param array $methodValues
     */
    protected function createMockBase($className, $methodMaps = array(), $methodValues = array())
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $mockBase = $this->getMockBuilder($className)
            ->disableOriginalConstructor()
            ->getMock();

        foreach ($methodMaps as $methodName => $results) {
            /** @noinspection PhpUndefinedMethodInspection */
            $mockBase->method($methodName)
                ->will($this->returnValueMap($results));
        }

        foreach ($methodValues as $methodName => $values) {
            /** @noinspection PhpUndefinedMethodInspection */
            $mockBase->method($methodName)
                ->willReturn($values);
        }

        return $mockBase;
    }
}
