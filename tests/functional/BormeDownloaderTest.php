<?php

/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 30/5/17
 * Time: 22:48
 */

use Borme\BormeDownloader;

class BormeDownloaderTest extends BaseTest
{
    /**
     * Comprobamos la descarga de un pdf válido
     */
    public function test01()
    {
        $borme = new BormeDownloader();
        $fileName = $borme->downloadBorme('https://www.boe.es/borme/dias/2017/01/10/pdfs/BORME-A-2017-6-41.pdf');
        $this->assertTrue(file_exists($fileName));
    }

    /**
     * Comprobamos la descarga de un pdf erróneo con ruta errónea, obtenemos excepcion
     * @expectedException Exception
     */
    public function test02()
    {
        $borme = new BormeDownloader();
        $borme->downloadBorme('https://www.boe.es/borme/diiiiias/2017/01/10/pdfs/BORME-A-2017-6-41.pdf');
    }
}