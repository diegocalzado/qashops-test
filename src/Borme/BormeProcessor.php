<?php

/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 29/5/17
 * Time: 22:53
 */
namespace Borme;

class BormeProcessor
{
    /**
     * @var BormeEntriesList
     */
    protected $entriesList;

    /**
     * BormeProcessor constructor.
     */
    public function __construct()
    {
        $this->entriesList = new BormeEntriesList();
    }

    /**
     * Obtener información de Borme
     * @param $rawBormeText
     * @return BormeEntriesList
     */
    public function getBormeInformation($rawBormeText)
    {
        $entries = $this->getEntriesFromRawText($rawBormeText);

        foreach ($entries as $entry) {
            $this->entriesList->setEntry($entry);
        }

        return $this->entriesList;
    }

    /**
     * Obtener un array de elementos BormeEntry
     * @param $rawBormeText
     * @return array
     * @throws \Exception
     */
    protected function getEntriesFromRawText($rawBormeText)
    {
        $entries = array();

        $matches = null;
        $pattern = '/\d{1,}\s-\s(.*)\n{1,}(.*)T\s*(\d{1,}).*F\s*(\d{1,}).*S\s*(\d{1,}).*H(.*),/';
        preg_match_all($pattern, $rawBormeText, $matches, PREG_SET_ORDER);

        if (!is_null($matches)) {
            foreach ($matches as $match) {
                $entry = new BormeEntry();
                $entry->setCompanyName(trim($match[1]))
                    ->setVolume(trim($match[3]))
                    ->setPage(trim($match[4]))
                    ->setSection(trim($match[5]))
                    ->setSheet(trim($match[6]));

                $entries[] = $entry;
            }
        } else {
            throw new \Exception('Parsing raw text error');
        }

        return $entries;
    }
}