<?php
/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 29/5/17
 * Time: 22:55
 */

namespace Borme;

use \Smalot\PdfParser\Parser;

class BormeDownloader
{
    const TMP_DIRECTORY = '/tmp';
    const PDF_EXTENSION = '.pdf';
    const TEXT_EXTENSION = '.txt';
    const DOWNLOAD_RETRIES = 5;
    const HTTP_STATUS_200 = 200;

    /** @var Parser */
    protected $pdfParser;
    protected $timestamp;

    /**
     * BormeDownloader constructor.
     */
    public function __construct()
    {
        $this->pdfParser = new Parser();
        $this->timestamp = microtime(true);
    }

    /**
     * Descargar borme y guardar su contenido en texto
     * @param $url
     * @return mixed|null
     * @throws \Exception
     */
    public function downloadBorme($url)
    {
        $textPath = null;

        $fileName = $this->getFileNameFromUrl($url);

        $retries = 0;
        do {
            $retries++;
            $content = $this->downloadFile($url);
        } while ($content === false && $retries < self::DOWNLOAD_RETRIES);

        if ($content !== false) {
            try {
                $pdfPath = $this->savePdfFile($content, $fileName);
                $pdf = $this->pdfParser->parseContent($content);
                $textPath = $this->saveTextFile($pdf->getText(), $fileName);
                $this->removeFile($pdfPath);
            } catch (\Exception $e) {
                // Esto está aquí porque PDF parser deja el buffer abierto al cascar y provoca risky test en phpunit
                ob_end_flush();
                throw $e;
            }
        } else {
            throw new \Exception("No se pudo descargar el pdf [$url]");
        }

        return $textPath;
    }

    /**
     * Descarga PDF usando cURL
     * @param $url
     * @return mixed
     */
    protected function downloadFile($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($httpcode != self::HTTP_STATUS_200) {
            return false;
        }

        return $result;
    }

    /**
     * Guarda un archivo
     * @param $content
     * @param $fileName
     * @return mixed
     * @throws \Exception
     */
    protected function saveFile($content, $fileName)
    {
        $fp = fopen($fileName, 'w');

        if ($fp === false) {
            throw new \Exception("Error al abrir el fichero [$fileName]");
        }

        if (fwrite($fp, $content) === false) {
            throw new \Exception("Error al escribir en el fichero [$fileName]");
        }

        if (fclose($fp) === false) {
            throw new \Exception("Error al cerrar el fichero [$fileName]");
        }

        return $fileName;
    }

    /**
     * Guardar pdf
     * @param $content
     * @param $fileName
     * @return mixed
     */
    protected function savePdfFile($content, $fileName)
    {
        $path = self::TMP_DIRECTORY . DIRECTORY_SEPARATOR . $fileName . '-' . $this->timestamp . self::PDF_EXTENSION;
        return $this->saveFile($content, $path);
    }

    /**
     * Guardar texto
     * @param $content
     * @param $fileName
     * @return mixed
     */
    protected function saveTextFile($content, $fileName)
    {
        $path = self::TMP_DIRECTORY . DIRECTORY_SEPARATOR . $fileName . '-' . $this->timestamp . self::TEXT_EXTENSION;
        return $this->saveFile($content, $path);
    }

    /**
     * Eliminar un archivo
     * @param $path
     */
    protected function removeFile($path)
    {
        unlink($path);
    }

    /**
     * Obtener el nombre del fichero desde la url
     * @param $url
     * @return mixed
     * @throws \Exception
     */
    protected function getFileNameFromUrl($url)
    {
        $matches = null;
        $pattern = '/\/(BORME-.*).pdf$/';
        preg_match($pattern, $url, $matches);

        if (count($matches) == 2) {
            return $matches[1];
        } else {
            throw new \Exception("No se pudo obtener el nombre del fichero desde [$url]");
        }
    }
}