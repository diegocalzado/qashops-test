<?php

/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 29/5/17
 * Time: 22:55
 */
namespace Borme;

class BormeEntry
{
    protected $companyName;
    protected $volume;
    protected $page;
    protected $section;
    protected $sheet;

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     * @return BormeEntry
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param mixed $volume
     * @return BormeEntry
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     * @return BormeEntry
     */
    public function setPage($page)
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param mixed $section
     * @return BormeEntry
     */
    public function setSection($section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSheet()
    {
        return $this->sheet;
    }

    /**
     * @param mixed $sheet
     * @return BormeEntry
     */
    public function setSheet($sheet)
    {
        $this->sheet = $sheet;
        return $this;
    }
}