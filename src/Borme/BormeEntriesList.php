<?php

/**
 * Created by PhpStorm.
 * User: Diego
 * Date: 29/5/17
 * Time: 22:55
 */
namespace Borme;

class BormeEntriesList implements \Iterator, \Countable
{
    private $position = 0;
    private $entries = array();

    /**
     * BormeEntriesList constructor.
     */
    public function __construct() {
        $this->position = 0;
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->entries[$this->position];
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        ++$this->position;
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return $this->position;
    }

    /**
     * @inheritDoc
     */
    public function valid()
    {
        return isset($this->entries[$this->position]);
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * Establecer un elemento BormeEntry
     * @param BormeEntry $entry
     */
    public function setEntry(BormeEntry $entry)
    {
        $this->entries[] = $entry;
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->entries);
    }
}